# What it is

Softbump is just my attempt at an effect I saw in a video for new UI styles. It was for an effect that looked like a thin rubber material being pushed up from underneath.

There are a few varients I tried for this effect. The first is a softer look. As if they rubber material were thicker
The second attempt was fora thiner material with sharper edges on the effect.
The the last was just a  translation into a night mode look.

Personally, I don't think I'd use this look on a website or app, however, its a not too bad.
This look might suit better as a hover effect. 

### Soft
![](screenshots/soft.png)

### Hard
![](screenshots/hard.png)

### Hard Night
![](screenshots/hard_night.png)